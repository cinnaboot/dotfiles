# don't run bwrap under firejail
# https://github.com/netblue30/firejail/issues/3647
env WEBKIT_FORCE_SANDBOX=0
