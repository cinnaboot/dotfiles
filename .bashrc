# If not running interactively, don't do anything
[[ "$-" != *i* ]] && return

if [ -d "$HOME/bin" ] ; then
	PATH="$HOME/bin:$PATH"
fi

export EDITOR=vim

# colors
reset="\[\e[0m\]"
red="\[\e[0;31m\]"
green="\[\e[0;32m\]"
yellow="\[\e[0;33m\]"
blue="\[\e[0;34m\]"
purple="\[\e[0;35m\]"
cyan="\[\e[0;36m\]"
light_red="\[\e[1;31m\]"
light_green="\[\e[1;32m\]"
light_yellow="\[\e[1;33m\]"
light_blue="\[\e[1;34m\]"
light_purple="\[\e[1;35m\]"
light_cyan="\[\e[1;36m\]"
light_gray="\[\e[0;37m\]"


#USER="\[\e[38;5;157m\]"
#HOST="\[\e[38;5;153m\]"
#DIRECTORY="\[\e[38;5;214m\]"
user_name=$green
host=$light_cyan
directory=$yellow

PS1="[$user_name\u$reset@$host\h$reset:$directory\w$reset]$ "

alias ls='ls -hF --color=tty --group-directories-first'                 # classify files in colour
alias ll='ls -l'                              # long list
alias grep='grep --color=always'
alias tmux='tmux -2'
alias gdb='gdb -q'
alias tree='tree -C --dirsfirst'
alias less='less -R'

alias pacman='pacman --color=auto'
alias yay='yay --color=auto'

alias gs='git status'
alias gap='git add --patch'

# Umask
#
# /etc/profile sets 022, removing write perms to group + others.
# Set a more restrictive umask: i.e. no exec perms for others:
# umask 027
# Paranoid: neither group nor others have any perms:
# umask 077

