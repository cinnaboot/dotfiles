"
" Begin Vundle config
" 	https://github.com/VundleVim/Vundle.vim
"
set nocompatible
filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'https://github.com/skywind3000/asyncrun.vim.git'
Plugin 'tikhomirov/vim-glsl'
Plugin 'octol/vim-cpp-enhanced-highlight'
Plugin 'godlygeek/tabular' " required for vim-markdown
Plugin 'plasticboy/vim-markdown'
Plugin 'https://github.com/StanAngeloff/php.vim.git'
Plugin 'https://github.com/pangloss/vim-javascript.git'
Plugin 'jlanzarotta/bufexplorer'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'https://github.com/HiPhish/jinja.vim'
Plugin 'https://github.com/ajmwagar/vim-deus'
Plugin 'https://github.com/tpope/vim-obsession'

" All of your Plugins must be added before the following line
call vundle#end()            " required

"
" End Vundle config
"


" some general vim settings
filetype plugin indent on
syntax on
colorscheme deus
" change default deus function color
hi! link Function deusAqua

set hlsearch
set nowrap
set linebreak
set mouse+=a
set noequalalways " don't auto resize windows when closing (eg: qf window)
set colorcolumn=80

" don't clutter directories with backup files
set nobackup
set nowritebackup
set tabstop=4
set shiftwidth=4	" set to same as tabstop for auto-indent
set clipboard=unnamedplus

" save undo buffer, make sure directory exists!
set undofile
set undodir=$HOME/.vim/undo
set undolevels=1000
set undoreload=10000

" folding
set foldmethod=syntax
set foldnestmax=1
set foldlevelstart=20
set foldcolumn=0
set autoindent
let php_folding = 2

" filename auto-completion
set wildmode=longest,list,full
set wildmenu

" attempts to fix delay when mode switch, also need tmux option	'set -s escape-time 0'
set ttimeoutlen=10

" line numbering gutter
set nonu

" override default paste behavior
xnoremap p pgvy

" gvim options
if has("gui_running")
	set lines=60 columns=100
endif

"------------------
" plugin configs

" bufexplorer
let g:bufExplorerSortBy='fullpath'
let g:bufExplorerDefaultHelp=0
let g:bufExplorerShowRelativePath=1
let g:bufExplorerSplitOutPathName=0

"------------------
" airline plugin stuffs
set laststatus=2
let g:airline_section_a = airline#section#create(['mode',' ','branch'])
let g:airline_section_b = airline#section#create(['hunks', 'branch'])
let g:airline_theme='bubblegum'
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_altsep = ''
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.linenr = ''
"let g:airline#extensions#tabline#enabled = 1

"------------------
" custom keybinds
"	C-PageUp/Down conflicts with xfce-terminal, so there is a trick to fixing
"	https://unix.stackexchange.com/questions/159979/xfce4-terminal-disable-individual-shortcut
"map <C-PageDown> :bnext<CR>
"map <C-PageUp> :bprev<CR>
" jump to word under cursor in a preview window
noremap <F2> :ptjump <c-r><c-w> <cr>
" search for word under cursor in current directory
noremap <F3> ::AsyncRun grep -RIn --exclude=tags --exclude-dir=build --exclude-dir=ext --exclude-dir=.git <c-r><c-w> <cr>
" close and delete current buffer
noremap <F4> :bp\|bd #<cr>
noremap <F5> :call RunMake()<cr>
noremap <F6> :call QfToggle() <cr>

"------------------
" AsyncRun
let g:asyncrun_timer = 250
let g:qf_toggle = ''
let g:asyncrun_prog = ''

function QfToggle()
	if (g:qf_toggle == '')
		call QfJumpAndReturn()
	else
		let g:qf_toggle = ''
		:ccl
	endif
endfunction

function QfJumpAndReturn()
	let g:qf_toggle = "open"
	:copen 16
	:wincmd p
endfunction

function RunMake()
	" update ctags?
	"	trying to update ctags blocks AsyncRun, maybe need autocommands?
	":AsyncRun -cwd=<root> ctags -R ./src/
	:AsyncRun -cwd=<root> -save=2 make -j4
	let g:asyncrun_prog = 'make'
endfunction

function CheckMakeStatus()
	if (g:asyncrun_prog == 'make')
		if (g:asyncrun_status == 'success')
			call QfToggle()
		endif

		let g:asyncrun_prog = ''
	endif
endfunction

"------------------
" autocommands
augroup vimrc
	" remove all vimrc autocommands
	autocmd!

	" custom copen
	autocmd User AsyncRunStart call QfJumpAndReturn()
	autocmd User AsyncRunStop call CheckMakeStatus()

	" Don't screw up folds when inserting text that might affect them, until
	" " leaving insert mode. Foldmethod is local to the window. Protect against
	" " screwing up folding when switching between windows.
	autocmd InsertEnter * if !exists('w:last_fdm') | let w:last_fdm=&foldmethod | setlocal foldmethod=manual | endif
	autocmd InsertLeave,WinLeave * if exists('w:last_fdm') | let &l:foldmethod=w:last_fdm | unlet w:last_fdm | endif

	" save folds/cursor positions of buffers
	" FIXME: uncommenting these is now deleting buffers when switching as of
	"	2021-11-16
	autocmd BufWinLeave *.* mkview
	autocmd BufWinEnter *.* silent loadview

	" glsl filetype plugin
	autocmd! BufNewFile,BufRead *.vs,*.fs set ft=glsl

	" read .h files as cpp
	autocmd! BufNewFile,BufRead *.h set ft=cpp

	" set foldmethod to indent for python files
	autocmd FileType python setlocal foldmethod=indent

augroup END


"------------------
" fix keycodes when running inside tmux
" http://tmux-users.narkive.com/M9tIqoa0/c-pageup-and-c-pagedown-seems-not-to-work-in-tmux
if &term =~ '^screen' && exists('$TMUX')
	" Mouse mode
	set ttymouse=xterm2

	" tmux sends xterm-style keys when xterm-keys option is on
	execute "set <xUp>=\e[1;*A"
	execute "set <xDown>=\e[1;*B"
	execute "set <xRight>=\e[1;*C"
	execute "set <xLeft>=\e[1;*D"
	execute "set <xHome>=\e[1;*H"
	execute "set <xEnd>=\e[1;*F"
	execute "set <Insert>=\e[2;*~"
	execute "set <Delete>=\e[3;*~"
	execute "set <PageUp>=\e[5;*~"
	execute "set <PageDown>=\e[6;*~"
	execute "set <xF1>=\e[1;*P"
	execute "set <xF2>=\e[1;*Q"
	execute "set <xF3>=\e[1;*R"
	execute "set <xF4>=\e[1;*S"
	execute "set <F5>=\e[15;*~"
	execute "set <F6>=\e[17;*~"
	execute "set <F7>=\e[18;*~"
	execute "set <F8>=\e[19;*~"
	execute "set <F9>=\e[20;*~"
	execute "set <F10>=\e[21;*~"
	execute "set <F11>=\e[23;*~"
	execute "set <F12>=\e[24;*~"
endif


" Testing airline/asyncrun colors ------------------

" " Define new accents
" function! AirlineThemePatch(palette)
"   " [ guifg, guibg, ctermfg, ctermbg, opts ].
"   See help attr-list for valid values for the opt value.
"   " http://vim.wikia.com/wiki/Xterm256_color_names_for_console_Vim
"   let a:palette.accents.running = [ '', '', '', '', '' ]
"   let a:palette.accents.success = [ '#00ff00', '' , 'green', '', '' ]
"   let a:palette.accents.failure = [ '#ff0000', '' , 'red', '', '' ]
" endfunction
" let g:airline_theme_patch_func = 'AirlineThemePatch'
"
"
" " Change color of the relevant section according to g:asyncrun_status, a global variable exposed by AsyncRun
" " 'running': default, 'success': green, 'failure': red
" let g:async_status_old = ''
" function! Get_asyncrun_running()
"
"   let async_status = g:asyncrun_status
"   if async_status != g:async_status_old
"
"     if async_status == 'running'
"       call airline#parts#define_accent('asyncrun_status', 'running')
"     elseif async_status == 'success'
"       call airline#parts#define_accent('asyncrun_status', 'success')
"     elseif async_status == 'failure'
"       call airline#parts#define_accent('asyncrun_status', 'failure')
"     endif
"
"     let g:airline_section_x = airline#section#create(['asyncrun_status'])
"     "AirlineRefresh
"     let g:async_status_old = async_status
"
"   endif
"
"   return async_status
"
" endfunction
"
" call airline#parts#define_function('asyncrun_status', 'Get_asyncrun_running')
" let g:airline_section_x = airline#section#create(['asyncrun_status'])

"let g:asyncrun_status = ''
"let g:airline_section_error = airline#section#create_right(['%{g:asyncrun_status}'])

"-------------------------------------

