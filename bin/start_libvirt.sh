#!/usr/bin/sh
# start libvirtd systemd service, and cleanup on exit


VIRSH_CMD="virsh -c qemu:///system"
NET_NAME="default"
LIBVIRT_CIDR=192.168.122.0/24


if [ $UID -ne 0 ]; then
	echo "must run as root"
	exit 1
fi

start_libvirt() {
	systemctl start libvirtd

	if [ "$($VIRSH_CMD net-list --inactive --name)" = $NET_NAME ]; then
		$VIRSH_CMD net-start $NET_NAME
	fi

	# allow DNS for libvirt
	iptables -A INPUT -d $LIBVIRT_CIDR -p udp -m udp --dport 53 -j ACCEPT
	iptables -A OUTPUT -s $LIBVIRT_CIDR -p udp -m udp --sport 53 -j ACCEPT

	# allow SSH for libvirt
	iptables -A INPUT -d $LIBVIRT_CIDR -p tcp -m tcp --sport 22 -j ACCEPT
	iptables -A OUTPUT -s $LIBVIRT_CIDR -p tcp -m tcp --dport 22 -j ACCEPT
}

cleanup () {
	echo "exiting"
	$VIRSH_CMD net-destroy $NET_NAME
	systemctl stop libvirtd.socket
	systemctl stop libvirtd
	# reset default nftables
	nft -f /etc/nftables.conf
}

# call cleanup function on exit signal
trap cleanup EXIT
start_libvirt
echo "waiting for ctrl-c to cleanup"

while true
do
	sleep 1
done

