#!/usr/bin/env sh

# 1440p
wmctrl -r :ACTIVE: -e 0,1260,0,1290,1379
# 1080p
#wmctrl -r :ACTIVE: -e '0,1005,0,906,1026'
# ??p
#wmctrl -r :ACTIVE: -e 0,615,0,1938,1378

SESSION='dev'

if [ ! -n "$TMUX_PANE" ]; then
	tmux -2 new -s $SESSION -d

#	tmux rename-window -t $SESSION dev
#	tmux splitw -v -l 60 -t $SESSION
#	tmux send-keys -t $SESSION "vim" Enter
#	tmux send-keys -t $SESSION ":80vs" Enter
#	tmux resizep -x 161 -t $SESSION
#	tmux selectp -t 0
#	tmux resizep -x 80 -t $SESSION
#	tmux splitw -v -l 24 -t $SESSION
#	tmux selectp -t 2
#
#	tmux send-keys -t $SESSION ":80vs" Enter
	tmux attach -t $SESSION
else
	echo "don't run in tmux session"
fi
