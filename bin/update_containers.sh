#!/usr/bin/env sh

DEBIAN_CONTAINERS=""
ALPINE_CONTAINERS=""

for c in ${ALPINE_CONTAINERS}; do
	if [ $(lxc list -f csv -c s ${c}) = "RUNNING" ]; then
		echo "------------------------"
		echo "updating $c\n"
		lxc exec $c -- apk upgrade -U;
	fi
done

for c in ${DEBIAN_CONTAINERS}; do
	if [ $(lxc list -f csv -c s ${c}) = "RUNNING" ]; then
		echo "------------------------"
		echo "updating $c\n"
		lxc exec $c -- apt update;
		lxc exec $c -- apt upgrade -y;
	fi
done

