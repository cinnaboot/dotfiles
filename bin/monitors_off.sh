#!/usr/bin/env sh
#
# NOTE: workaround for steam bug where steam constantly inhibits screensaver
#	via dbus messages
#
# more details: https://github.com/ValveSoftware/steam-for-linux/issues/5607

xset dpms force off &&
sleep 15 &&
xset dpms force off

