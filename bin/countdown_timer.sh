#!/bin/bash

PLAYSOUND=true
PLAYER=mpv
FILE="/usr/share/sounds/freedesktop/stereo/message.oga"

seconds=${1:-180}
clear
echo "Counting down from $seconds"
Msg=${2}

let "i = $seconds"
while [ $i -gt 0 ]; do
	echo -ne "\r$i        "
	let i--
	sleep 1
done

notify-send --icon=diaglog-information "Countdown Timer finished!" "${Msg}"

echo "${Msg}"
echo ""
echo ""

if ($PLAYSOUND); then
	$PLAYER $FILE
fi
