#!/bin/sh
#
# NOTE: use with user cron, eg)
# 0 */2 * * * $HOME/bin/pacman_notify.sh

PKGS=$(checkupdates)
uid=1000
export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$uid/bus

if [ $(echo "$PKGS" | wc -w) -gt 0 ]; then
	notify-send "$(echo "$PKGS" | wc -l) updates available" "$PKGS"
fi
