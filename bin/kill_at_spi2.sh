#!/usr/bin/sh
#
# killall_at_spi2.sh
#
#	hack to fix an issue with xfce on arch linux where systemd hangs on reboot
#	see:
#		https://bbs.archlinux.org/viewtopic.php?id=276570
#		https://bbs.archlinux.org/viewtopic.php?pid=2032321

sleep 5

PID=$(pgrep -f at-spi-bus-launcher)

if [ $PID ]; then
	kill -s KILL $PID
fi
