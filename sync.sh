#!/bin/bash


DOTFILES="
	$HOME/.bashrc \
	$HOME/.ctags.d \
	$HOME/.gdbinit \
	$HOME/.gdbinit.d \
	$HOME/.tmux.conf \
	$HOME/.vimrc \
	$HOME/.xinitrc \
	"

rsync -avP ${DOTFILES} ./


VIM_EXTRA="
	$HOME/.vim/after/syntax/cpp.vim \
	"

rsync -avP ${VIM_EXTRA} .vim/after/syntax


APP_CONFIGS="
	$HOME/.config/conky \
	$HOME/.config/firejail \
	$HOME/.config/mpv
	"

rsync -avP ${APP_CONFIGS} .config/


SCRIPTS="
	$HOME/bin/countdown_timer.sh \
	$HOME/bin/dev.sh \
	$HOME/bin/get_bat.sh \
	$HOME/bin/hide_panel.sh \
	$HOME/bin/iommu.sh \
	$HOME/bin/kill_at_spi2.sh \
	$HOME/bin/monitors_off.sh \
	$HOME/bin/pacman_notify.sh \
	$HOME/bin/start_libvirt.sh \
	$HOME/bin/wmresize.sh \
	"

rsync -avP ${SCRIPTS} ./bin

