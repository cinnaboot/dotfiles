syn keyword cTodo NOTE
syn keyword Type uint u8 u16 u32 u64 i8 i16 i32 i64 f32 f64
syn keyword Type GLfloat GLenum GLint GLuint
syn keyword Type vec2 vec3 vec4 mat2 mat3 mat4 dmat2 dmat3 dmat4
syn keyword Type ivec2 ivec3 ivec4 uvec2 uvec3 uvec4 dvec2 dvec3 dvec4
